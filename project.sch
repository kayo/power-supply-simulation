EESchema Schematic File Version 2
LIBS:kicad-spice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L C C4
U 1 1 55003FCC
P 8000 4200
F 0 "C4" H 8000 4350 50  0000 C CNN
F 1 "100nF" H 8000 4050 50  0000 C CNN
F 2 "" H 8038 4050 30  0000 C CNN
F 3 "" H 8000 4200 60  0000 C CNN
	1    8000 4200
	0    -1   1    0   
$EndComp
$Comp
L C C2
U 1 1 55004159
P 4600 4700
F 0 "C2" H 4600 4850 50  0000 C CNN
F 1 "33nF" H 4600 4550 50  0000 C CNN
F 2 "" H 4638 4550 30  0000 C CNN
F 3 "" H 4600 4700 60  0000 C CNN
	1    4600 4700
	0    -1   -1   0   
$EndComp
$Comp
L C C1
U 1 1 55004379
P 4600 2600
F 0 "C1" H 4600 2450 50  0000 C CNN
F 1 "1nF" H 4600 2750 50  0000 C CNN
F 2 "" H 4638 2450 30  0000 C CNN
F 3 "" H 4600 2600 60  0000 C CNN
	1    4600 2600
	0    1    1    0   
$EndComp
Text Notes -1800 4150 0    60   ~ 0
-pspice\n* \n.MODEL QMJE13003 NPN\n+ IS=3.38476e-13\n+ BF=70.4395\n+ NF=1.46002\n+ VAF=713.551\n+ IKF=0.2266\n+ ISE=2.74992e-11\n+ NE=2.22158\n+ BR=3.83365\n+ NR=1.38008\n+ VAR=126.023\n+ IKR=0.100002\n+ ISC=9.43838e-14\n+ NC=2.29404\n+ RB=10\n+ IRB=0.2\n+ RBM=10\n+ RE=0.0001\n+ RC=0.061224\n+ XTB=1.35204\n+ XTI=3.03161\n+ EG=1.206\n+ CJE=4.7174e-10\n+ VJE=0.43016\n+ MJE=0.295041\n+ TF=1e-08\n+ XTF=1.76126\n+ VTF=4.70624\n+ ITF=0.001\n+ CJC=7.38617e-11\n+ VJC=0.4\n+ MJC=0.359328\n+ XCJC=0.794116\n+ FC=0.8\n+ TR=3.2234u\n+ Vce=400\n+ Icrating=1.5
$Comp
L D D1
U 1 1 55004940
P 4600 3600
F 0 "D1" H 4600 3750 50  0000 C CNN
F 1 "D1N4007" H 4600 3450 50  0000 C CNN
F 2 "" H 4600 3600 60  0000 C CNN
F 3 "" H 4600 3600 60  0000 C CNN
	1    4600 3600
	0    -1   -1   0   
$EndComp
$Comp
L DB XB1
U 1 1 55004C9E
P 4900 3900
F 0 "XB1" H 4900 4050 50  0000 C CNN
F 1 "DB3" H 4900 3750 50  0000 C CNN
F 2 "" H 4900 3900 60  0000 C CNN
F 3 "" H 4900 3900 60  0000 C CNN
	1    4900 3900
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 550055B5
P 4000 2300
F 0 "R1" H 4000 2400 50  0000 C CNN
F 1 "330K" H 4000 2200 50  0000 C CNN
F 2 "" V 3930 2300 30  0000 C CNN
F 3 "" H 4000 2300 30  0000 C CNN
	1    4000 2300
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 550058F1
P 4300 2300
F 0 "R3" H 4300 2400 50  0000 C CNN
F 1 "220K" H 4300 2200 50  0000 C CNN
F 2 "" V 4230 2300 30  0000 C CNN
F 3 "" H 4300 2300 30  0000 C CNN
	1    4300 2300
	0    -1   -1   0   
$EndComp
$Comp
L R R6
U 1 1 55005C23
P 5700 5300
F 0 "R6" H 5700 5400 50  0000 C CNN
F 1 "20" H 5700 5200 50  0000 C CNN
F 2 "" V 5630 5300 30  0000 C CNN
F 3 "" H 5700 5300 30  0000 C CNN
	1    5700 5300
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 55005CBA
P 5800 4700
F 0 "R8" H 5800 4800 50  0000 C CNN
F 1 "2.2" H 5800 4600 50  0000 C CNN
F 2 "" V 5730 4700 30  0000 C CNN
F 3 "" H 5800 4700 30  0000 C CNN
	1    5800 4700
	0    -1   -1   0   
$EndComp
$Comp
L R R7
U 1 1 55006045
P 5800 2900
F 0 "R7" H 5800 3000 50  0000 C CNN
F 1 "2.2" H 5800 2800 50  0000 C CNN
F 2 "" V 5730 2900 30  0000 C CNN
F 3 "" H 5800 2900 30  0000 C CNN
	1    5800 2900
	0    -1   -1   0   
$EndComp
$Comp
L R R5
U 1 1 55006105
P 5700 1500
F 0 "R5" H 5700 1600 50  0000 C CNN
F 1 "20" H 5700 1400 50  0000 C CNN
F 2 "" V 5630 1500 30  0000 C CNN
F 3 "" H 5700 1500 30  0000 C CNN
	1    5700 1500
	1    0    0    -1  
$EndComp
$Comp
L D D6
U 1 1 55006602
P 6200 2100
F 0 "D6" H 6200 2250 50  0000 C CNN
F 1 "D1N4007" H 6200 1950 50  0000 C CNN
F 2 "" H 6200 2100 60  0000 C CNN
F 3 "" H 6200 2100 60  0000 C CNN
	1    6200 2100
	0    -1   -1   0   
$EndComp
$Comp
L D D7
U 1 1 55006713
P 6200 3900
F 0 "D7" H 6200 4050 50  0000 C CNN
F 1 "D1N4007" H 6200 3750 50  0000 C CNN
F 2 "" H 6200 3900 60  0000 C CNN
F 3 "" H 6200 3900 60  0000 C CNN
	1    6200 3900
	0    -1   -1   0   
$EndComp
Text GLabel 6800 4100 1    60   Input ~ 0
FBH
Text GLabel 6800 4300 3    60   Input ~ 0
FBL
Text GLabel 7600 3300 1    60   Input ~ 0
CMP
$Comp
L R R9
U 1 1 550089F8
P 9800 5000
F 0 "R9" H 9800 5100 50  0000 C CNN
F 1 "10" H 9800 4900 50  0000 C CNN
F 2 "" V 9730 5000 30  0000 C CNN
F 3 "" H 9800 5000 30  0000 C CNN
	1    9800 5000
	0    -1   -1   0   
$EndComp
Text GLabel 9800 4400 2    60   Input ~ 0
DCO
$Comp
L D D2
U 1 1 5500B03F
P 9300 1700
F 0 "D2" H 9300 1850 50  0000 C CNN
F 1 "D1N4007" H 9300 1550 50  0000 C CNN
F 2 "" H 9300 1700 60  0000 C CNN
F 3 "" H 9300 1700 60  0000 C CNN
	1    9300 1700
	-1   0    0    -1  
$EndComp
$Comp
L D D3
U 1 1 5500B105
P 9300 2100
F 0 "D3" H 9300 2250 50  0000 C CNN
F 1 "D1N4007" H 9300 1950 50  0000 C CNN
F 2 "" H 9300 2100 60  0000 C CNN
F 3 "" H 9300 2100 60  0000 C CNN
	1    9300 2100
	-1   0    0    -1  
$EndComp
$Comp
L D D4
U 1 1 5500B13E
P 9300 2500
F 0 "D4" H 9300 2650 50  0000 C CNN
F 1 "D1N4007" H 9300 2350 50  0000 C CNN
F 2 "" H 9300 2500 60  0000 C CNN
F 3 "" H 9300 2500 60  0000 C CNN
	1    9300 2500
	1    0    0    -1  
$EndComp
$Comp
L D D5
U 1 1 5500B1F6
P 9300 2900
F 0 "D5" H 9300 3050 50  0000 C CNN
F 1 "D1N4007" H 9300 2750 50  0000 C CNN
F 2 "" H 9300 2900 60  0000 C CNN
F 3 "" H 9300 2900 60  0000 C CNN
	1    9300 2900
	1    0    0    -1  
$EndComp
Text GLabel 7600 1700 1    60   Input ~ 0
DCI
$Comp
L C C3
U 1 1 5500C170
P 8700 2300
F 0 "C3" H 8700 2450 50  0000 C CNN
F 1 "3.3uF" H 8700 2150 50  0000 C CNN
F 2 "" H 8738 2150 30  0000 C CNN
F 3 "" H 8700 2300 60  0000 C CNN
	1    8700 2300
	0    -1   -1   0   
$EndComp
Text GLabel 4000 3900 0    60   Input ~ 0
P0
Text GLabel 5200 4300 0    60   Input ~ 0
B2
Text GLabel 5200 2100 0    60   Input ~ 0
B1
Text GLabel 7600 5000 0    60   Input ~ 0
T0
Text GLabel 9900 1700 1    60   Input ~ 0
AC1
Text GLabel 9700 2500 3    60   Input ~ 0
AC0
Text GLabel 5800 2500 0    60   Input ~ 0
E1
Text GLabel 5800 4300 0    60   Input ~ 0
E2
Text GLabel 8000 4700 0    60   Input ~ 0
T1
Text Notes 8450 4100 0    60   ~ 0
-pspice\n* \n.SUBCKT TR2 1 2 3 4\nE 5 4 1 2 0.056\nF 1 2 VM 0.056\nVM 5 6\nRP 1 2 10\nRS 6 3 10u\n.ENDS
$Comp
L T XT1
U 1 1 5501640D
P 8350 4900
F 0 "XT1" H 8350 5200 50  0000 C CNN
F 1 "TR2" H 8350 4700 50  0000 C CNN
F 2 "" H 8350 4900 60  0000 C CNN
F 3 "" H 8350 4900 60  0000 C CNN
	1    8350 4900
	1    0    0    -1  
$EndComp
Text Notes 7000 2900 0    60   ~ 0
-pspice\n* \nK1 L1 L2 0.9999\nK2 L2 L3 0.9999\nK3 L3 L1 0.9999
Text Notes 2250 3750 0    60   ~ 0
-pspice\n* \n.SUBCKT DB3 2 1\n* TERMINALS: MT2 MT1 \nQN1  5 4 2  NOUT; OFF \nQN2  8 6 7  NOUT; OFF \nQP1  6 8 10  POUT; OFF \nQP2  4 5 9  POUT; OFF \nD1  7 9  DZ \nD2  2 10 DZ \nDF  4 3  DZ; OFF \nDR  6 3  DZ; OFF \nRF  4 3  1.13E+7 \nRR  6 3  1.13E+7 \nRT2 1 7  0.755 \nRH  7 6  10k \nRH2 4 2  10k \n.MODEL DZ D\n+ IS=321F\n+ RS=100\n+ N=1.5\n+ IBV=10N\n+ BV=30.3\n.MODEL POUT PNP\n+ IS=321F\n+ BF=100\n+ CJE=134p\n+ TF=25.5U\n.MODEL NOUT NPN\n+ IS=321F\n+ BF=200\n+ CJE=134p\n+ CJC=26.8p\n+ TF=1.7U\n.ENDS
Text Notes 800  2100 0    60   ~ 0
-pspice\n* \n.MODEL D1N4007 D\n+ is = 1.09774E-008\n+ n = 1.78309\n+ rs = 0.0414388\n+ eg = 1.11\n+ xti = 3\n+ cjo = 2.8173E-011\n+ vj = 0.50772\n+ m = 0.318974\n+ fc = 0.5\n+ tt = 9.85376E-006\n+ bv = 1100\n+ ibv = 0.1\n+ af = 1\n+ kf = 0
Text Notes 4400 7550 0    60   ~ 0
+pspice\n* \n.control\n  options nopage\n  tran 100uS 20mS 0mS\n  set gnuplot_terminal=png\n  gnuplot project\n  + v(dci)\n  + v(e1,b1)\n  + v(e2,b2)\n  + v(t0,t1)\n  + v(o0,o1)\n  + v(dco)\n  let pwr=v(ac0,ac1)*i(V1)\n  meas tran pwrtest avg pwr\n  + from=5mS\n  + to=20mS\n  meas tran vouttest avg v(dco)\n  + from=5mS\n  + to=20mS\n.endc
Wire Wire Line
	4600 3700 4600 4600
Wire Wire Line
	4000 3900 4800 3900
Connection ~ 4600 3900
Wire Wire Line
	4600 4800 4600 5200
Wire Wire Line
	4600 2700 4600 3500
Wire Wire Line
	4300 3300 7600 3300
Connection ~ 4600 3300
Wire Wire Line
	4300 3300 4300 3100
Wire Wire Line
	4300 1700 4300 2100
Wire Wire Line
	4000 1700 9200 1700
Wire Wire Line
	5000 3900 5500 3900
Wire Wire Line
	5200 3900 5200 5300
Connection ~ 5200 3900
Wire Wire Line
	5800 4100 5800 4500
Wire Wire Line
	5800 4900 5800 5100
Wire Wire Line
	5500 2100 5200 2100
Wire Wire Line
	5200 2100 5200 1500
Wire Wire Line
	5800 2300 5800 2700
Wire Wire Line
	5800 3100 5800 3700
Connection ~ 5800 3300
Wire Wire Line
	5800 1700 5800 1900
Wire Wire Line
	6200 1700 6200 2000
Wire Wire Line
	6200 2200 6200 3800
Connection ~ 6200 3300
Wire Wire Line
	8500 5000 9300 5000
Wire Wire Line
	9100 4400 9800 4400
Wire Wire Line
	9800 4400 9800 4800
Wire Wire Line
	9800 5200 9800 5700
Wire Wire Line
	8700 1700 8700 2200
Wire Wire Line
	8700 2100 9200 2100
Wire Wire Line
	8700 2500 9200 2500
Wire Wire Line
	8700 2400 8700 3000
Wire Wire Line
	8700 2900 9200 2900
Wire Wire Line
	9400 1700 9900 1700
Wire Wire Line
	9900 1700 9900 2900
Wire Wire Line
	9900 2900 9400 2900
Connection ~ 8700 1700
Connection ~ 8700 2900
Connection ~ 8700 2100
Connection ~ 8700 2500
Wire Wire Line
	4000 3900 4000 3100
Connection ~ 4300 1700
Wire Wire Line
	7600 3300 7600 4000
Wire Wire Line
	6600 4100 7100 4100
Wire Wire Line
	6600 4300 7100 4300
Wire Wire Line
	4600 5100 7100 5100
Wire Wire Line
	7600 5000 8200 5000
Connection ~ 7100 3300
Wire Wire Line
	5200 5300 5500 5300
Wire Wire Line
	6600 5300 6600 4300
Wire Wire Line
	7100 3300 7100 3600
Wire Wire Line
	6600 1500 6600 4100
Wire Wire Line
	7100 5100 7100 4800
Wire Wire Line
	7100 4300 7100 4400
Wire Wire Line
	7100 4100 7100 4000
Wire Wire Line
	7600 4400 7600 5000
Connection ~ 5800 1700
Wire Wire Line
	4000 1700 4000 2100
$Comp
L QNPN Q1
U 1 1 550D5E9A
P 5700 2100
F 0 "Q1" H 5700 2300 50  0000 C CNN
F 1 "QMJT50T4" V 5900 2100 50  0000 C CNN
F 2 "" H 5700 2100 60  0000 C CNN
F 3 "" H 5700 2100 60  0000 C CNN
	1    5700 2100
	1    0    0    -1  
$EndComp
$Comp
L V V1
U 1 1 550D5ECD
P 10450 2300
F 0 "V1" V 10250 2300 60  0000 C CNN
F 1 "SIN(0V 240V 50Hz)" V 10650 2300 60  0000 C CNN
F 2 "" H 10450 2300 60  0000 C CNN
F 3 "" H 10450 2300 60  0000 C CNN
	1    10450 2300
	-1   0    0    -1  
$EndComp
Text GLabel 10450 2000 1    60   Input ~ 0
AC1
Text GLabel 10450 2600 3    60   Input ~ 0
AC0
Wire Wire Line
	10450 2500 10450 2600
Wire Wire Line
	10450 2100 10450 2000
Wire Wire Line
	9400 2100 9700 2100
Wire Wire Line
	9700 2100 9700 2500
Wire Wire Line
	9700 2500 9400 2500
$Comp
L L L3
U 1 1 550D8070
P 7600 4200
F 0 "L3" H 7600 4300 50  0000 C CNN
F 1 "80uH" H 7600 4150 50  0000 C CNN
F 2 "" H 7600 4200 60  0000 C CNN
F 3 "" H 7600 4200 60  0000 C CNN
	1    7600 4200
	0    -1   -1   0   
$EndComp
$Comp
L L L1
U 1 1 550D841C
P 7100 3800
F 0 "L1" H 7100 3900 50  0000 C CNN
F 1 "8uH" H 7100 3750 50  0000 C CNN
F 2 "" H 7100 3800 60  0000 C CNN
F 3 "" H 7100 3800 60  0000 C CNN
	1    7100 3800
	0    1    1    0   
$EndComp
$Comp
L L L2
U 1 1 550D8470
P 7100 4600
F 0 "L2" H 7100 4700 50  0000 C CNN
F 1 "8uH" H 7100 4550 50  0000 C CNN
F 2 "" H 7100 4600 60  0000 C CNN
F 3 "" H 7100 4600 60  0000 C CNN
	1    7100 4600
	0    1    1    0   
$EndComp
Connection ~ 5800 5100
Wire Wire Line
	6200 5100 6200 4000
$Comp
L QNPN Q2
U 1 1 550D9CE1
P 5700 3900
F 0 "Q2" H 5700 4100 50  0000 C CNN
F 1 "QMJT50T4" V 5900 3900 50  0000 C CNN
F 2 "" H 5700 3900 60  0000 C CNN
F 3 "" H 5700 3900 60  0000 C CNN
	1    5700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2500 4600 1700
Connection ~ 4600 1700
Wire Wire Line
	5200 1500 5500 1500
Wire Wire Line
	5900 1500 6600 1500
$Comp
L 0 #GND01
U 1 1 550DBA6D
P 4600 5200
F 0 "#GND01" H 4600 5100 40  0001 C CNN
F 1 "0" H 4600 5130 40  0000 C CNN
F 2 "" H 4600 5200 60  0000 C CNN
F 3 "" H 4600 5200 60  0000 C CNN
	1    4600 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 5600 9800 5600
$Comp
L 0 #GND02
U 1 1 550DBD39
P 9800 5700
F 0 "#GND02" H 9800 5600 40  0001 C CNN
F 1 "0" H 9800 5630 40  0000 C CNN
F 2 "" H 9800 5700 60  0000 C CNN
F 3 "" H 9800 5700 60  0000 C CNN
	1    9800 5700
	1    0    0    -1  
$EndComp
Connection ~ 9800 5600
Connection ~ 6200 5100
$Comp
L R R4
U 1 1 550CFF29
P 4300 2900
F 0 "R4" H 4300 3000 50  0000 C CNN
F 1 "220K" H 4300 2800 50  0000 C CNN
F 2 "" V 4230 2900 30  0000 C CNN
F 3 "" H 4300 2900 30  0000 C CNN
	1    4300 2900
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 550D01CD
P 4000 2900
F 0 "R2" H 4000 3000 50  0000 C CNN
F 1 "330K" H 4000 2800 50  0000 C CNN
F 2 "" V 3930 2900 30  0000 C CNN
F 3 "" H 4000 2900 30  0000 C CNN
	1    4000 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4000 2500 4000 2700
Wire Wire Line
	4300 2500 4300 2700
$Comp
L 0 #GND03
U 1 1 550D563D
P 8700 3000
F 0 "#GND03" H 8700 2900 40  0001 C CNN
F 1 "0" H 8700 2930 40  0000 C CNN
F 2 "" H 8700 3000 60  0000 C CNN
F 3 "" H 8700 3000 60  0000 C CNN
	1    8700 3000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5900 5300 6600 5300
Connection ~ 4600 5100
Wire Wire Line
	8200 4700 8000 4700
Wire Wire Line
	8000 4700 8000 4300
Wire Wire Line
	8000 1700 8000 4100
Connection ~ 6200 1700
Text Notes 2250 6400 0    60   ~ 0
-pspice\n* \n.SUBCKT SS14 1 2\nddio 1 2 legd\ndgr 1 2 grd\n.MODEL legd D\n+ is = 1E-009\n+ n = 0.625832\n+ rs = 0.0619933\n+ eg = 0.745352\n+ xti = 0.5\n+ cjo = 2.83445E-010\n+ vj = 0.739175\n+ m = 0.484673\n+ fc = 0.5\n+ tt = 1.4427E-009\n+ bv = 44\n+ ibv = 0.5\n+ af = 1\n+ kf = 0\n.MODEL grd D\n+ is = 1E-008\n+ n = 1.73481\n+ rs = 0.0283094\n+ eg = 1.8\n+ xti = 4\n.ENDS
$Comp
L D XD1
U 1 1 550DB870
P 9000 4400
F 0 "XD1" H 9000 4525 60  0000 C CNN
F 1 "SS14" H 9000 4275 60  0000 C CNN
F 2 "" H 9000 4400 60  0000 C CNN
F 3 "" H 9000 4400 60  0000 C CNN
	1    9000 4400
	1    0    0    -1  
$EndComp
$Comp
L D XD2
U 1 1 550DB910
P 9000 5600
F 0 "XD2" H 9000 5725 60  0000 C CNN
F 1 "SS14" H 9000 5475 60  0000 C CNN
F 2 "" H 9000 5600 60  0000 C CNN
F 3 "" H 9000 5600 60  0000 C CNN
	1    9000 5600
	-1   0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 550DC924
P 9300 4700
F 0 "C5" H 9300 4825 60  0000 C CNN
F 1 "100uF" H 9300 4575 60  0000 C CNN
F 2 "" H 8650 5250 60  0000 C CNN
F 3 "" H 8650 5250 60  0000 C CNN
	1    9300 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9300 4800 9300 5200
$Comp
L C C6
U 1 1 550DCDA2
P 9300 5300
F 0 "C6" H 9300 5425 60  0000 C CNN
F 1 "100uF" H 9300 5175 60  0000 C CNN
F 2 "" H 8650 5850 60  0000 C CNN
F 3 "" H 8650 5850 60  0000 C CNN
	1    9300 5300
	0    -1   -1   0   
$EndComp
Connection ~ 9300 5000
Wire Wire Line
	9300 4600 9300 4400
Connection ~ 9300 4400
Wire Wire Line
	9300 5400 9300 5600
Connection ~ 9300 5600
Connection ~ 8000 1700
Text GLabel 8700 4400 1    60   Input ~ 0
O1
Text GLabel 9000 5000 1    60   Input ~ 0
O0
Wire Wire Line
	8700 5600 8900 5600
Wire Wire Line
	8700 4400 8700 5600
Wire Wire Line
	8700 4400 8900 4400
Wire Wire Line
	8500 4700 8700 4700
Connection ~ 8700 4700
Text GLabel 4000 2600 0    60   Input ~ 0
P1
Text GLabel 4300 2600 0    60   Input ~ 0
P2
Text Notes -550 3150 0    60   ~ 0
-pspice\n* \n.MODEL QTIP50 NPN\n+ IS=103.43E-15\n+ BF=54.506\n+ VAF=100\n+ IKF=1.1424\n+ ISE=1.9338E-12\n+ NE=1.4142\n+ BR=474.59\n+ VAR=100\n+ IKR=18.269\n+ ISC=61.843E-9\n+ NC=2.1043\n+ NK=.56313\n+ RB=.65638\n+ RC=.26576\n+ CJE=1.0117E-9\n+ VJE=.6676\n+ MJE=.34403\n+ CJC=110.89E-12\n+ VJC=.41121\n+ MJC=.38646\n+ TF=7.5319E-9\n+ XTF=4.9255\n+ VTF=11.221\n+ ITF=1.6898\n+ TR=10.000E-9
Text Notes 800  6150 0    60   ~ 0
-pspice\n* \n.MODEL QMJT50T4 NPN\n+ IS=7.25559e-11\n+ BF=26.7185\n+ NF=1.5\n+ VAF=25.4206\n+ IKF=6.11331\n+ ISE=4.74986e-12\n+ NE=3.46876\n+ BR=2.67185\n+ NR=1.44677\n+ VAR=3.40787\n+ IKR=3.07918\n+ ISC=4.75e-13\n+ NC=3.96875\n+ RB=0.911238\n+ IRB=0.1\n+ RBM=0.1\n+ RE=0.000647444\n+ RC=0.138596\n+ XTB=0.130702\n+ XTI=1\n+ EG=1.13492\n+ CJE=3.02579e-09\n+ VJE=0.649273\n+ MJE=0.351387\n+ TF=6.6943e-09\n+ XTF=1.5\n+ VTF=1\n+ ITF=0.999999\n+ CJC=3.0004e-10\n+ VJC=0.600008\n+ MJC=0.409967\n+ XCJC=0.8\n+ FC=0.534143\n+ CJS=0\n+ VJS=0.75\n+ MJS=0.5\n+ TR=1e-07\n+ PTF=0\n+ KF=0\n+ AF=1
$EndSCHEMATC
